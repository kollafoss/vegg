extends Node2D

# Nodes
var timer
var label

onready var sound = get_node("Sound")
onready var game = get_tree().get_root().get_node("game")

# Timer Parameters
var time_left
var timer_active = true

func _ready():
	set_process(true)
	if global.game_debug:
		game.start_game()
		queue_free()

	timer = get_node("Timer")
	label = get_node("Label")
	timer.connect("timeout", self, "_on_Timer_timeout")

func restart_timer():
	timer.start()
	timer_active = true

func _on_Timer_timeout():
	game.start_game()
	queue_free()

func _process(delta):
	if(timer_active):
		var new_time = int(timer.get_time_left())
		if(new_time != time_left):
			sound.play(0)
			time_left = new_time

			label.set_text(str(time_left+1))