extends Node2D

var grow = true
var size = Vector2(1,1)
func _ready():
	set_process(true)
	pass

func _process(delta):
	rotate(0.5*delta)
	if grow:
		apply_scale(Vector2(1.005,1.005))
		if get_scale() > Vector2(1.2,1.2):
			grow = false
	else:
		apply_scale(Vector2(0.99,0.99))
		if get_scale() < Vector2(1.0,1.0):
			grow = true
