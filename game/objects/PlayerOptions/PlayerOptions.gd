extends Node2D

onready var global = get_node("/root/global")

var player = preload("res://objects/player/player.tscn")
var restartable = false
var player_count = 0
var game_resolution

# Player Parameters
var player_number = 0
var gamepad = false

var color = Color(0.1, 0.1, 0.1)

var player_ready = 0

var countdown = false
var timeleft

# Shake
onready var current_scene = get_node("/root/global").current_scene()
onready var camera = current_scene.get_node("Camera")
onready var sound = get_node("Sound")


func _ready():
	set_process_input(true)
	set_process(true)
	game_resolution = Vector2(ProjectSettings.get("display/window/width"),ProjectSettings.get("display/window/height"))

	if global.game_debug:
		spawn_player(0, true)
		spawn_player(1, true)
		spawn_player(2, true)

func _process(delta):
	if countdown:
		if timeleft != int(get_node("StartTimer").get_time_left())+1:
			timeleft = int(get_node("StartTimer").get_time_left())+1
			get_node("startzone/Label").set_text(str(timeleft))

func _input(event):
	if event.device < 4:
		if (event is InputEventKey or event is InputEventJoypadButton):
			if(event.is_pressed()):
				if(!restartable):
					if(event is InputEventJoypadButton):
						player_number = event.device
						gamepad = true
					else:
						player_number = 3
						gamepad =  false
	
					if(player_count == 0):
						spawn_player(player_number, gamepad)
					elif(!is_player_in(player_number) || player_count == 0):
						spawn_player(player_number, gamepad)
				else:
					get_tree().reload_current_scene()

func spawn_player(player_number, gamepad):
	global.players_gamepad[player_number] = gamepad
	global.players_in[player_number] = true
	
	var player_instance = player.instance()
	var random_location = Vector2(randi() % (int(game_resolution.x)-100), randi() % (int(game_resolution.y)-100))
	random_location = random_location + Vector2(50,50)
	
	if global.game_debug:
		random_location = Vector2(randi() % 50, randi() % 50)
		random_location = random_location + Vector2(game_resolution.x/2-25,game_resolution.y/2-25)

	get_node("players").add_child(player_instance)
	player_instance.add_to_group(str("player",player_number))
	player_instance.position = random_location
	
	player_instance.setup(player_number, gamepad)

	player_count = player_count + 1

	get_node("StartTimer").stop()
	get_node("TextTimer").start()
	if countdown == true:
		get_node("startzone/Label").set_text("ready?")

	countdown = false

func is_player_in(player_number):
	var players = get_node("players")
	if players.get_child_count() > 0:
		for player in (players.get_children()):
			if player.get_player_number() == player_number:
				return(true)
	return(false)

func _draw():
	draw_startzone( color )

func draw_startzone( color ):
	var size = 100
	var colors = PoolColorArray([color])
	draw_circle(Vector2(game_resolution.x/2,game_resolution.y/2), size, color)

func _on_startzone_body_enter( body ):
	if body.is_in_group("player"):
		sound.play()
		if camera != null:
			camera.shake(10)
		player_ready = player_ready+1
		if (player_ready == player_count && player_count > 1):
			countdown = true
			get_node("TextTimer").stop()
			get_node("StartTimer").start()

func _on_startzone_body_exit( body ):
	if body.is_in_group("player"):
		player_ready = player_ready-1
		countdown = false
		get_node("StartTimer").stop()
		get_node("startzone/Label").set_text("press any button")
		get_node("TextTimer").start()
		sound.play()
		if camera != null:
			camera.shake(5)

func _on_StartTimer_timeout():
	global.goto_scene("res://objects/game/game.tscn")

func _on_TextTimer_timeout():
	var label = get_node("startzone/Label")

	if (label.get_text() == "ready?"):
		label.set_text("Press any button")
	else:
		label.set_text("ready?")