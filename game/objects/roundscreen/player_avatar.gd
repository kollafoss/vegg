extends Node2D

var color = [ \
	Color(0.91, 0.47, 0.94), \
	Color(0.33, 0.75, 0.72), \
	Color(0.95, 0.72, 0.04), \
	Color(0.99, 0.28, 0.19)]

onready var player_number = get_node("../").player_number
var player_size = 20

func _draw():
	draw_triangle( color[player_number] )

func draw_triangle( color ):
	var points = PoolVector2Array()
	var colors = PoolColorArray([color])

	points.push_back(Vector2(-(player_size/2),-player_size/2))
	points.push_back(Vector2((player_size/2),-player_size/2))
	points.push_back(Vector2(0,player_size/2))

	draw_polygon(points, colors)

