extends Node2D

var player_score = preload("res://objects/roundscreen/player_score.tscn")
onready var root = get_node("/root/global")


func _ready():
	var pos_multiplier = 0
	for i in range(4):
		if root.players_in[i]:
			var player_score_instance = player_score.instance()
			player_score_instance.setup(i)
			add_child(player_score_instance)

			player_score_instance.position =+ Vector2(0,40*pos_multiplier)

			pos_multiplier += 1
