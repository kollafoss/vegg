extends Node2D

var color = Color(0.2, 0.2, 0.2)

var player_number = 0

var wall_segment_size = 4
var half_wall_segment_size = wall_segment_size/2

func change_color(new_color):
	color = new_color

func _draw():
	draw_wall( color )

func draw_wall( color ):
	var colors = PoolColorArray([color])
	var points = PoolVector2Array()

	points.push_back(Vector2(-half_wall_segment_size, -half_wall_segment_size))
	points.push_back(Vector2(half_wall_segment_size, -half_wall_segment_size))
	points.push_back(Vector2(half_wall_segment_size, half_wall_segment_size*4))
	points.push_back(Vector2(-half_wall_segment_size, half_wall_segment_size*4))

	draw_polygon(points, colors)