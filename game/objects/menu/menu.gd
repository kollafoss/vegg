extends Control

onready var global = get_node("/root/global")

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_Play_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	global.goto_scene("res://objects/PlayerOptions/PlayerOptions.tscn")

func _on_Settings_pressed():
	pass

func _on_Quit_pressed():
	global.quit()