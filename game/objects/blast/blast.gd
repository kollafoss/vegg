extends Area2D

onready var sound = get_node("Sound")
onready var current_scene = global.current_scene()

var timer

# Player Parameters
var player_number = 0
var color
var deadly = true

# Blast Parameters
var shake_power = 10
var blast_size = 50

func _ready():
	set_timer()

func _draw():
	draw_blast( color )

func setup(player, player_color, bsize, shake):
	player_number = player
	color = player_color
	blast_size = bsize
	shake_power = shake
	
	get_node("CollisionShape2D").get_shape().set_radius(blast_size)

func set_timer():
	timer = get_node("Timer")
	timer.connect("timeout", self, "_on_Timer_timeout")
	timer.set_wait_time(0.05)
	timer.start()

func _on_Timer_timeout():
	queue_free()

func draw_blast( color ):
	var camera = current_scene.get_node("Camera")
	if camera != null:
		sound.play(0)
		camera.shake(shake_power)
		var colors = PoolColorArray([color])
		draw_circle(Vector2(0,0), blast_size, color)

func _on_blast_body_entered(body):
	if body.is_in_group("wall"):
		body.die(0)
	if body.is_in_group("player"):
		body.die()