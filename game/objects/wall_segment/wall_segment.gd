extends StaticBody2D

# Wall Parameters
var hot = true
var blast_size = 20
var charge = 0

# Player Parameters
var start_color = Color(1.0, 1.0, 1.0)
var color = Color(0.0, 0.0, 1.0)
var player = 0

var blast = preload("res://objects/blast/blast.tscn")
onready var current_scene = get_node("/root/global").current_scene()

onready var wall_generator = get_node("../")

func _ready():
	pass

func setup(player_number, player_color, wall_charge):
	player = player_number
	color = player_color
	charge = wall_charge
	
func is_hot():
	return hot

func set_color():
	get_node("Graphics").set_color(color)

func blast():
	var blast_instance = blast.instance()
	blast_size = blast_size + charge
	blast_instance.setup(player, color, blast_size, 1)
	blast_instance.add_to_group(str("player",player))
	blast_instance.position = global_position
	current_scene.call_deferred("add_child", blast_instance)

func die(blast):
	if blast:
		blast()
		wall_generator.stop()
	self.queue_free()

func _on_hot_timer_timeout():
	hot = false
	set_color()
	get_node("Area2D").queue_free()

func _on_Area2D_body_enter( body ):
	if body != self && body != null:
		if body.is_in_group("wall"):
			body.charge = wall_generator.wall_charge
			body.die(1)
			wall_generator.charge(3)
		if body.is_in_group("player") and hot:
			body.die()


func _on_Area2D_area_enter( area ):
	pass
