extends Node2D

onready var sound = get_node("Sound")
onready var current_scene = global.current_scene()

# Player Parameters
var player_number = 0
var color

# Blast Parameters
var shake_power = 10

func _draw():
	draw_blast( color )

func setup(player, player_color):
	player_number = player
	color = player_color

func _on_Timer_timeout():
	queue_free()

func draw_blast( color ):
	var camera = current_scene.get_node("Camera")
	if camera != null:
		sound.play(0)
		camera.shake(shake_power)
		var colors = PoolColorArray([color])
		draw_circle(Vector2(0,0), 50, color)