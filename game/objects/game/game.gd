extends Node2D

#onready var global = get_node("/root/global")

# Preload scenes
var player = preload("res://objects/player/player.tscn")

# Game Parameters
var game_resolution
var kill_count = 0
var player_count = 0

# Player Parameters
var player_dead = [false,false,false,false]
var player_number = 0
var gamepad = false

# Timers
var restart_timer
var restart_time = 1.5

func _ready():
	global.game_on = true
	game_resolution = get_viewport().size

	restart_timer = get_node("RestartableTimer")
	restart_timer.connect("timeout", self, "_on_Restart_Timer_timeout")
	restart_timer.set_wait_time( restart_time )

	randomize()

func spawn_players():
	var players
	var players_gamepad
	
	if global.game_debug:
		global.players_in = [true,true,true,true]
		global.players_gamepad = [true,true,true,false]
	
	players = global.players_in
	players_gamepad = global.players_gamepad
	
	var player_count = 0
	for n in range(players.size()):
		if players[n]:
			spawn_player(n, players_gamepad[n])

func start_game():
	spawn_players()

func spawn_player(player_number, gamepad):
	if (!player_dead[player_number]):
		var player_instance = player.instance()
		var spawnpoint = get_node(str("Spawnpoints/Spawnpoint ",player_number)).position
		spawnpoint = Vector2(spawnpoint.x + ((randi() % 200) - 100),spawnpoint.y + ((randi() % 100) - 50))

		get_node("players").add_child(player_instance)
		player_instance.add_to_group(str("player",player_number))
		player_instance.position = spawnpoint

		player_instance.setup(player_number, gamepad)

		player_count = player_count + 1
		if global.player_score[player_number] == null:
			global.player_score[player_number] = 0
		global.player_round_score[player_number] = 0

func is_player_in(player_number):
	var players = get_node("players")
	if players.get_child_count() > 0:
		for player in (players.get_children()):
			if player.get_player_number() == player_number:
				return(true)
	return(false)

func is_player_dead(player_number):
	return player_dead[player_number]

func kill_player(player):
	if(!player_dead[player]):
		player_dead[player] = true
		kill_count = kill_count + 1
		if kill_count >= player_count-1:
			end_match()

func end_match():
	global.game_on = false

	var players = global.players_in
	for n in range(players.size()):
		if !player_dead[n] and players[n]:
			global.player_round_score[n] += 1

	restart_timer.start()

func _on_Restart_Timer_timeout():
	get_node("/root/global").game_on = false
	get_node("/root/global").round_number += 1
	if get_node("/root/global").gametype == 0:
		get_node("/root/global").goto_scene("res://objects/roundscreen/roundscreen.tscn")
	elif get_node("/root/global").gametype == 1:
		get_node("/root/global").goto_scene("res://objects/roundscreen/roundscreen.tscn")