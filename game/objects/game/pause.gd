extends Node2D

var areyousure = false

func _ready():
	set_process_unhandled_input(true)

func _unhandled_input(event):
	if event.is_action_pressed("pause"):
		if !self.is_visible_in_tree():
			self.show()
			get_tree().set_pause(true)
		else:
			self.hide()
			get_tree().set_pause(false)
			
	if event.is_action_pressed("ui_cancel") and !areyousure:
		get_node("Label1").set_text("Are you sure? Press O to confirm")
		get_node("Timer").start()
		areyousure = true
	
	elif event.is_action_pressed("ui_accept") and areyousure:
		global.player_score = [null,null,null,null]
		global.round_number = 1
		global.players_in = [false,false,false,false]
		global.players_gamepad = [false,false,false,false]
		global.highest_score = 0
		
		get_tree().set_pause(false)
		global.goto_scene("res://objects/menu/menu.tscn")

func _on_Timer_timeout():
	areyousure = false
	get_node("Label1").set_text("Press A to quit")
	
