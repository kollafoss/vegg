extends Camera2D

var shake = 10

func _ready():
	set_process(false)

func _process(delta):
	if shake != 0:
		var random_shake = Vector2( \
		rand_range(-2.0, 2.0) * shake, \
		rand_range(-2.0, 2.0) * shake)
		set_offset(random_shake)
		
		if shake > 0:
			shake -= 1
		elif shake <= 0:
			shake = 0
	else:
		set_process(false)

func shake(power):
	set_process(true)
	shake = power