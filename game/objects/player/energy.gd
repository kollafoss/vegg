extends Node2D

# Player Parameters
var color
var player

# Energy Bar Parameters
var energy
var player_scale
var charged_color = Color(1, 1, 1)

# Utilities
var pick_color = true

func _ready():
	set_process(true)
	player_scale = get_scale()
	player = get_node("../")
	pass

func _process(delta):
	energy = float(player.get_energy())
	
	if(energy < 100):
		get_node("Graphics").set_color(player.get_player_color())
	else:
		get_node("Graphics").set_color(charged_color)
	
	energy = energy/100
	set_scale(Vector2(energy,player_scale.y))