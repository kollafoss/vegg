 extends KinematicBody2D

# Preloading scenes
var wall = preload("res://objects/wall_generator/wall_generator.tscn")
var sword = preload("res://objects/sword/sword.tscn")
var blast = preload("res://objects/blast/blast.tscn")
var joinblast = preload("res://objects/joinblast/joinblast.tscn")
var win_token = preload("res://objects/WinToken/WinToken.tscn")

onready var sound = get_node("Sound")

# Player Parameters
var player_number = 0
var player_velocity = Vector2()
var player_speed = 300
onready var color = get_node("/root/global").player_color
var safe = true

# Controls
var gamepad = true
export var deadzone = 0.20
export var outer_deadzone = 0.80
export var aim_deadzone = 0.80
onready var deadzone_scale = 1/(outer_deadzone-deadzone)

# Weapons
var shooting = false
var slashing = false
var wall_reloading = false
var wall_reload_time = 0.01
var energy = 0
var sword_instance
var sword_energy_cost = 99

# Timers
var wall_timer
var sword_timer
var death_timer
var death_time = 0.05
var sword_safe_timer

var winning = true

onready var current_scene = get_node("/root/global").current_scene()

func _ready():
	wall_timer = get_node("WallTimer")
	wall_timer.connect("timeout", self, "_on_Wall_Timer_timeout")
	wall_timer.set_wait_time( wall_reload_time )

	death_timer = get_node("DeathTimer")
	death_timer.connect("timeout", self, "_on_Death_Timer_timeout")
	death_timer.set_wait_time( death_time )

	sword_timer = get_node("SwordTimer")
	sword_timer.connect("timeout", self, "_on_Sword_Timer_timeout")

	sword_safe_timer = get_node("SwordSafeTimer")
	sword_safe_timer.connect("timeout", self, "_on_Sword_Safe_Timer_timeout")

	sound.play(0)


func _process(delta):
	if shooting:
		shoot()


func _physics_process(delta):
	if((!shooting and !slashing) and energy < 100):
		energy = (energy + 1) * 1.03

	if Input.is_action_pressed(str("player",player_number,"_left")):
		player_velocity.x = -Input.get_action_strength(str("player",player_number,"_left"))
	elif Input.is_action_pressed(str("player",player_number,"_right")):
		player_velocity.x = Input.get_action_strength(str("player",player_number,"_right"))
	else:
		player_velocity.x = 0

	if Input.is_action_pressed(str("player",player_number,"_up")):
		player_velocity.y = -Input.get_action_strength(str("player",player_number,"_up"))
	elif Input.is_action_pressed(str("player",player_number,"_down")):
		player_velocity.y = Input.get_action_strength(str("player",player_number,"_down"))
	else:
		player_velocity.y = 0

	if(!gamepad):
		if !slashing:
			var mpos = get_viewport().get_mouse_position()
			look_at(mpos)
	
	player_velocity = deadzone(player_velocity)
	var motion = (player_velocity.clamped(1) * player_speed)
	
	if slashing:
		var direction = Vector2(cos(rotation), sin(rotation))
		motion = (direction.normalized() * player_speed * 4)

	move_and_slide(motion)

func _unhandled_input(event):
	var direction = Vector2(0,0)
	# Aiming
	if !slashing:
		direction.x = Input.get_action_strength(str("player",player_number,"_aim_right")) \
			- Input.get_action_strength(str("player",player_number,"_aim_left"))
		direction.y = Input.get_action_strength(str("player",player_number,"_aim_down")) \
			- Input.get_action_strength(str("player",player_number,"_aim_up"))
		if !((direction.x <= aim_deadzone && direction.y <= aim_deadzone) \
			&& (direction.x >= -aim_deadzone && direction.y >= -aim_deadzone)):
			rotate_player(direction)

	# Weapons
	if (energy >= 100 and event.is_action_pressed(str("player",player_number,"_fire"))):
		shooting = true
	if event.is_action_released(str("player",player_number,"_fire")):
		shooting = false
	if event.is_action_pressed(str("player",player_number,"_slash")):
		slash()

func setup(player, using_gamepad):
	player_number = player
	gamepad = using_gamepad
	joinblast()

	if get_node("/root/global").round_number > 1:
		if get_node("/root/global").player_score[player_number] == get_node("/root/global").highest_score:
			var win_token_instance = win_token.instance()

			self.add_child(win_token_instance)
			win_token_instance.add_to_group(str("player",player_number))
			win_token_instance.position = Vector2(-30,0)
			win_token_instance.setup(player_number, color[player_number], self)

	get_node("Graphics").set_color(color[player])

func die():
	if get_node("/root/global").game_on and !safe:
		if !current_scene.is_player_dead(player_number):
			if !slashing:
				blast()
				current_scene.kill_player(player_number)
				death_timer.start()

func rotate_player(direction):
	rotation = direction.angle()

func deadzone(joy_axis):
	if (joy_axis.length() < deadzone || joy_axis.length() > outer_deadzone):

		# Check X Deadzones
		if (joy_axis.x < deadzone && joy_axis.x > -deadzone):
			joy_axis.x = 0
		elif (joy_axis.x > outer_deadzone || joy_axis.x < -outer_deadzone):
			if (joy_axis.x > 0):
				joy_axis.x = outer_deadzone
			else:
				joy_axis.x = -outer_deadzone

		# Check Y Deadzones
		if (joy_axis.y < deadzone && joy_axis.y > -deadzone):
			joy_axis.y = 0
		elif (joy_axis.y > outer_deadzone || joy_axis.y < -outer_deadzone):
			if (joy_axis.y > 0):
				joy_axis.y = outer_deadzone
			else:
				joy_axis.y = -outer_deadzone

	# Scale X to 0 and 1
	if (joy_axis.x > deadzone):
		joy_axis.x = ((joy_axis.x - deadzone) * deadzone_scale )
	elif (joy_axis.x < -deadzone):
		joy_axis.x = ((joy_axis.x + deadzone) * deadzone_scale )

	# Scale Y to 0 and 1
	if (joy_axis.y > deadzone):
		joy_axis.y = ((joy_axis.y - deadzone) * deadzone_scale )
	elif (joy_axis.y < -deadzone):
		joy_axis.y = ((joy_axis.y + deadzone) * deadzone_scale )

	return joy_axis

func _on_Wall_Timer_timeout():
	wall_reloading = false

func _on_Death_Timer_timeout():
	queue_free()

func _on_Sword_Timer_timeout():
	slashing = false
	get_node("Graphics").set_color(color[player_number])
	sword_instance.destroy()
	
func _on_Sword_Safe_Timer_timeout():
	set_collision_mask_bit(0, true)
	set_collision_layer_bit(0, true)

func shoot():
	if(!wall_reloading and energy >= 100):
		Input.start_joy_vibration(player_number, 0.8,0.5,0.05)
		var player_position = global_position
		var player_rotation = rotation
		var wall_instance = wall.instance()

		energy = 0
		get_node("AnimationPlayer").play("Shoot")
		$"Walls".add_child(wall_instance)
		wall_instance.add_to_group(str("player",player_number))
		wall_instance.rotation = player_rotation
		wall_instance.position = player_position
		wall_instance.setup(player_number, rotation, color[player_number])

		wall_reloading = true
		wall_timer.start()

func slash():
	if(energy > sword_energy_cost):
		
		get_node("AnimationPlayer").play("Dash")
		Input.start_joy_vibration(player_number, 0.3,0,0.1)
		get_node("Graphics").set_color(Color(1,1,1))
	
		set_collision_layer_bit(0, false)
		set_collision_mask_bit(0, false)
		slashing = true
		energy = 0
		sword_instance = sword.instance()
		add_child(sword_instance)
		sword_instance.add_to_group(str("player",player_number))
		sword_instance.setup(player_number, color[player_number])
		sword_timer.start()
		sword_safe_timer.start()
		get_node("SwordParticles").set_emitting(true)

func get_player_number():
	return player_number

func get_player_color():
	return color[player_number]

func blast():
	Input.start_joy_vibration(player_number, 1,1,0.05)
	var blast_instance = blast.instance()
	blast_instance.setup(player_number, color[player_number], 50, 10)
	blast_instance.add_to_group(str("player",player_number))
	call_deferred("add_child", blast_instance)

func joinblast():
	Input.start_joy_vibration(player_number, 1,1,0.05)
	var join_blast_instance = joinblast.instance()
	join_blast_instance.setup(player_number, color[player_number])
	join_blast_instance.add_to_group(str("player",player_number))
	call_deferred("add_child", join_blast_instance)

func get_energy():
	return(energy)

func _on_SafeTimer_timeout():
	safe = false
	$SafeShield.visible = false
