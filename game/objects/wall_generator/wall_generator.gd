extends Node2D

onready var sound = get_node("Sound")

# Scene Preload
var wall_segment = preload("res://objects/wall_segment/wall_segment.tscn")

# Wall Parameters
var wall_gap = 16
var wall_range = 100
var wall_charge = 0

# Utilities
var counter = 0
var timer

# Player Parameters
var player_number = 0
var color
var player_rotation = Vector2(0,0)

var generate_walls = true


onready var spawnpoint = get_node("spawnpoint")

func _ready():
	sound.play(0)
	set_process(true)

func charge(in_charge):
	wall_charge = wall_charge + in_charge

func _process(delta):
	if (counter < wall_range) && generate_walls:
		if !get_node("spawnpoint/VisibilityNotifier2D").is_on_screen():
			stop()
		else:
			for i in range(2):
				if generate_walls:
					var wall_segment_instance = wall_segment.instance()
					wall_segment_instance.setup(player_number, color, wall_charge)
					wall_segment_instance.add_to_group(str("player",player_number))
					call_deferred("add_child", wall_segment_instance)
					wall_segment_instance.position = spawnpoint.position
					spawnpoint.move_local_x(wall_gap)
					counter = counter + 1

func setup(number, rot, player_color):
	player_number = number
	player_rotation = rot
	color = player_color
	
	var flash = get_node("flash")
	flash.add_to_group(str("player",player_number))

func stop():
	generate_walls = false

func _on_flash_body_enter( body ):
	if body.is_in_group("player") and !body.is_in_group(str("player",player_number)):
		body.die()

func _on_Timer_timeout():
	get_node("flash").queue_free()

func _on_Area2D_body_enter( body ):
	if body != null:
		if body.is_in_group("wall") and !body.is_in_group(str("player",player_number)):
			body.die(1)
			stop()
