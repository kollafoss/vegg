extends Area2D

onready var sound = get_node("Sound")

# Player Parameters
var player_number = 0
var color

func _ready():
	sound.play(0)

func setup(player, player_color):
	player_number = player
	color = player_color

func destroy():
	self.queue_free()

func _on_sword_body_entered(body):
	if (body.is_in_group("player") and !body.is_in_group(str("player",player_number))):
		body.die()
	elif (body.is_in_group("wall")):
		body.die(1)